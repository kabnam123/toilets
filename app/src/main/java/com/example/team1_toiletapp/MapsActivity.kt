package com.example.team1_toiletapp

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.thecodecity.navigation.directionhelpers.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {


    private lateinit var mMap: GoogleMap
    private val LOCATION_PERMISSION_REQUEST = 1
    var start: MarkerOptions = MarkerOptions().position(LatLng(56.937451, 24.238647)).title("location 1")
    var end: MarkerOptions = MarkerOptions().position(LatLng(56.951993, 24.179634)).title("location 2")
    var currentPolyline: Polyline? = null

    private fun getLocationAccess() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        }
        else
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                mMap.isMyLocationEnabled = true
            }
            else {
                Toast.makeText(this, "User has not granted location access permission", Toast.LENGTH_LONG).show()
                finish()
            }
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        getLocationAccess()
        mMap.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
            override fun onMapClick(latLng: LatLng) {
                val markerOptions = MarkerOptions()
                markerOptions.position(latLng)
                markerOptions.title("" + latLng.latitude + " : " + latLng.longitude).icon((BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
                googleMap.addMarker(markerOptions)
            }
        })

    }





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        start = MarkerOptions().position(LatLng(56.937451, 24.238647))
            .title("location 1")
        end = MarkerOptions().position(LatLng(56.951993, 24.179634))
            .title("location 2")

        val url: String = getUrl(start.getPosition(), end.getPosition(), "walking") // <----


        FetchURL(this@MapsActivity).execute(url, "walking")
    }

    private fun getUrl(
        Start: LatLng,
        End: LatLng,
        Mode: String
    ): String {                                   // <----
        // Origin of route
        val str_origin = "origin=" + Start.latitude + "," + Start.longitude
        // Destination of route
        val str_dest = "destination=" + End.latitude + "," + End.longitude
        // Mode
        val mode = "mode=$Mode"
        // Build parameters to the web service
        val parameters = "$str_origin&$str_dest&$mode"
        // Output format
        val output = "json"
        //Buil the URL to the web service
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" +  //
                parameters + "&key=" + getString(R.string.google_maps_key)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nearest -> Toast.makeText(this, "Nearest toilet found", Toast.LENGTH_SHORT).show()
            R.id.add -> Toast.makeText(this, "Toilet added", Toast.LENGTH_SHORT).show()
            R.id.review -> Toast.makeText(this, "Review", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    fun onTaskDone(vararg values: Any?) {                                                      // <----
        if (currentPolyline != null) currentPolyline!!.remove()
        currentPolyline = mMap.addPolyline(values[0] as PolylineOptions?)
    }

}