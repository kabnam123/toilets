package com.thecodecity.navigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import com.example.team1_toiletapp.R;
import com.example.team1_toiletapp.databinding.ActivityMapsBinding;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.thecodecity.navigation.directionhelpers.FetchURL;
import com.thecodecity.navigation.directionhelpers.TaskLoadedCallback;

// https://github.com/Vysh01/android-maps-directions

class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, TaskLoadedCallback {

    private GoogleMap map;
    /* TODO
        is this what we need in our project
        everythig else isn't necessary

        MarkerOptions start, end;     are the points of start and end of road;
        Polyline currentPolyline;    is route itself

        String url = getUrl(start.getPosition(), end.getPosition(), "walking");
            this gets information for Direction API to build the route

        new FetchURL(MapsActivity.this).execute(url, "walking");
            this send info to Direction API services, gets response and builds the route

        public void onTaskDone(Object... values)
            and this adds polyline to map
     */
    MarkerOptions start, end;
    Polyline currentPolyline;                                                                       // <----

    SupportMapFragment supportMapFragment;
    FusedLocationProviderClient client;

    private ActivityMapsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        // Initialize client
        client = LocationServices.getFusedLocationProviderClient(this);

        //check permission
        if (ActivityCompat.checkSelfPermission(MapsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            // if permission granted
            // Call method
            getCurrentLocation();
        }else{
            // if permission denied
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }



        start = new MarkerOptions().position(new LatLng(56.937451, 24.238647))
                .title("location 1");
        end = new MarkerOptions().position(new LatLng(56.951993, 24.179634))
                .title("location 2");

        String url = getUrl(start.getPosition(), end.getPosition(), "walking");               // <----

        new FetchURL(MapsActivity.this).execute(url, "walking");                            // <----


    }

    private String getUrl(LatLng Start, LatLng End, String Mode){                                   // <----
        // Origin of route
        String str_origin = "origin=" + Start.latitude + "," + Start.longitude;
        // Destination of route
        String str_dest = "destination=" + End.latitude + "," + End.longitude;
        // Mode
        String mode = "mode=" + Mode;
        // Build parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        //Buil the URL to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + //
                parameters + "&key=" + getString(R.string.google_maps_key);
        return url;
    }

    private void getCurrentLocation() {
        // Initialize task location
        Task<Location> task = client.getLastLocation();     // permission is checked in 'onCreate'
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // when success
                if (location != null) {
                    // Sync map
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(@NonNull GoogleMap googleMap) {
                            // Initialize lat lng
                            LatLng latLng = new LatLng(location.getLatitude(),
                                    location.getLongitude());
                            // Create marker option
                            MarkerOptions option = new MarkerOptions().position(latLng)
                                    .title("I'm here");

                            start = option;

                            //Zoom map
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,
                                    14));
                            // Add marker on map
                            googleMap.addMarker(option);
                        }
                    });
                }
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 44) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                // When permission granted
                // Call method
                getCurrentLocation();
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

//        // Add a marker in Riga and move the camera
//        LatLng riga = new LatLng(56.9476, 24.1031);
//        map.addMarker(new MarkerOptions().position(riga).title("Marker in Riga"));
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(riga, 15));
    }


    /* Need this one */
    @Override
    public void onTaskDone(Object... values) {                                                      // <----
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = map.addPolyline((PolylineOptions) values[0]);
    }
}

//class FetchURL extends AsyncTask<String, Void, String> {
//    Context mContext;
//    String directionMode = "driving";
//
//    public FetchURL(Context mContext) {
//        this.mContext = mContext;
//    }
//
//    @Override
//    protected String doInBackground(String... strings) {
//        // For storing data from web service
//        String data = "";
//        directionMode = strings[1];
//        try {
//            // Fetching the data from web service
//            data = downloadUrl(strings[0]);
//            Log.d("mylog", "Background task data " + data.toString());
//        } catch (Exception e) {
//            Log.d("Background Task", e.toString());
//        }
//        return data;
//    }
//
//    @Override
//    protected void onPostExecute(String s) {
//        super.onPostExecute(s);
//        PointsParser parserTask = new PointsParser(mContext, directionMode);
//        // Invokes the thread for parsing the JSON data
//        parserTask.execute(s);
//    }
//
//    private String downloadUrl(String strUrl) throws IOException {
//        String data = "";
//        InputStream iStream = null;
//        HttpURLConnection urlConnection = null;
//        try {
//            URL url = new URL(strUrl);
//            // Creating an http connection to communicate with url
//            urlConnection = (HttpURLConnection) url.openConnection();
//            // Connecting to url
//            urlConnection.connect();
//            // Reading data from url
//            iStream = urlConnection.getInputStream();
//            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
//            StringBuffer sb = new StringBuffer();
//            String line = "";
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//            }
//            data = sb.toString();
//            Log.d("mylog", "Downloaded URL: " + data.toString());
//            br.close();
//        } catch (Exception e) {
//            Log.d("mylog", "Exception downloading URL: " + e.toString());
//        } finally {
//            iStream.close();
//            urlConnection.disconnect();
//        }
//        return data;
//    }
//}
//
//class PointsParser extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
//    TaskLoadedCallback taskCallback;
//    String directionMode = "driving";
//
//    public PointsParser(Context mContext, String directionMode) {
//        this.taskCallback = (TaskLoadedCallback) mContext;
//        this.directionMode = directionMode;
//    }
//
//    // Parsing the data in non-ui thread
//    @Override
//    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
//
//        JSONObject jObject;
//        List<List<HashMap<String, String>>> routes = null;
//
//        try {
//            jObject = new JSONObject(jsonData[0]);
//            Log.d("mylog", jsonData[0].toString());
//            DataParser parser = new DataParser();
//            Log.d("mylog", parser.toString());
//
//            // Starts parsing data
//            routes = parser.parse(jObject);
//            Log.d("mylog", "Executing routes");
//            Log.d("mylog", routes.toString());
//
//        } catch (Exception e) {
//            Log.d("mylog", e.toString());
//            e.printStackTrace();
//        }
//        return routes;
//    }
//
//    // Executes in UI thread, after the parsing process
//    @Override
//    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
//        ArrayList<LatLng> points;
//        PolylineOptions lineOptions = null;
//        // Traversing through all the routes
//        for (int i = 0; i < result.size(); i++) {
//            points = new ArrayList<>();
//            lineOptions = new PolylineOptions();
//            // Fetching i-th route
//            List<HashMap<String, String>> path = result.get(i);
//            // Fetching all the points in i-th route
//            for (int j = 0; j < path.size(); j++) {
//                HashMap<String, String> point = path.get(j);
//                double lat = Double.parseDouble(point.get("lat"));
//                double lng = Double.parseDouble(point.get("lng"));
//                LatLng position = new LatLng(lat, lng);
//                points.add(position);
//            }
//            // Adding all the points in the route to LineOptions
//            lineOptions.addAll(points);
//            if (directionMode.equalsIgnoreCase("walking")) {
//                lineOptions.width(10);
//                lineOptions.color(Color.MAGENTA);
//            } else {
//                lineOptions.width(20);
//                lineOptions.color(Color.BLUE);
//            }
//            Log.d("mylog", "onPostExecute lineoptions decoded");
//        }
//
//        // Drawing polyline in the Google Map for the i-th route
//        if (lineOptions != null) {
//            //mMap.addPolyline(lineOptions);
//            taskCallback.onTaskDone(lineOptions);
//
//        } else {
//            Log.d("mylog", "without Polylines drawn");
//        }
//    }
//}
//
//interface TaskLoadedCallback {
//    void onTaskDone(Object... values);
//}
//
//class DataParser {
//    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
//
//        List<List<HashMap<String, String>>> routes = new ArrayList<>();
//        JSONArray jRoutes;
//        JSONArray jLegs;
//        JSONArray jSteps;
//        try {
//            jRoutes = jObject.getJSONArray("routes");
//            /** Traversing all routes */
//            for (int i = 0; i < jRoutes.length(); i++) {
//                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
//                List path = new ArrayList<>();
//                /** Traversing all legs */
//                for (int j = 0; j < jLegs.length(); j++) {
//                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
//
//                    /** Traversing all steps */
//                    for (int k = 0; k < jSteps.length(); k++) {
//                        String polyline = "";
//                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
//                        List<LatLng> list = decodePoly(polyline);
//
//                        /** Traversing all points */
//                        for (int l = 0; l < list.size(); l++) {
//                            HashMap<String, String> hm = new HashMap<>();
//                            hm.put("lat", Double.toString((list.get(l)).latitude));
//                            hm.put("lng", Double.toString((list.get(l)).longitude));
//                            path.add(hm);
//                        }
//                    }
//                    routes.add(path);
//                }
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//        }
//        return routes;
//    }
//
//
//    /**
//     * Method to decode polyline points
//     * Courtesy : https://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
//     */
//    private List<LatLng> decodePoly(String encoded) {
//
//        List<LatLng> poly = new ArrayList<>();
//        int index = 0, len = encoded.length();
//        int lat = 0, lng = 0;
//
//        while (index < len) {
//            int b, shift = 0, result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lat += dlat;
//
//            shift = 0;
//            result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lng += dlng;
//
//            LatLng p = new LatLng((((double) lat / 1E5)),
//                    (((double) lng / 1E5)));
//            poly.add(p);
//        }
//
//        return poly;
//    }
//}